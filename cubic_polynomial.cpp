//
// Created by walce on 06.11.2022.
//

#include "cubic_polynomial.h"

double cubic_polynomial::operator()(double x) {
    double x_ = x - coeff.x;
    return coeff.a + coeff.b * x_ + coeff.c / 2 * std::pow(x_, 2) + coeff.d / 6 * std::pow(x_, 3);
}

cubic_polynomial::cubic_polynomial(const coefficients &coeff) : coeff(coeff) {}
