//
// Created by walce on 06.11.2022.
//

#ifndef NUMERICAL_METHOD_CUBIC_POLYNOMIAL_H
#define NUMERICAL_METHOD_CUBIC_POLYNOMIAL_H

#include "cmath"

struct coefficients {
    double a;
    double b;
    double c;
    double d;
    double x;
};

class cubic_polynomial {
private:
    coefficients coeff;

public:
    cubic_polynomial(const coefficients &coeff = {0, 0, 0, 0, 0});

    double operator()(double x);
};

#endif //NUMERICAL_METHOD_CUBIC_POLYNOMIAL_H
