//
// Created by walcermelodia on 09.10.2022.
//
#include "lab1.h"
#include <iostream>
double round_significant_digits(double numb, int count) {
    if (numb == 0.0)
        return 0.0;

    double factor = pow(10.0, count - ceil(log10(fabs(numb))));
    return round(numb * factor) / factor;
}

std::pair<double, double> task_1(double numb, int count_round) {
    double round_numb = round_significant_digits(numb, count_round);

    return std::pair<double, double>(
            std::abs(numb - round_numb),
            std::abs(numb - round_numb) / abs(round_numb));
}

double task_2(double approximate_numb, double relative_err) {
    return relative_err * approximate_numb;
}

size_t task_3(double approximate_numb, double absolute_err) {
    double class_numb = calc_max_class_numb((approximate_numb));

    size_t count_corr_numb = 0;
    while(2 * absolute_err <= class_numb) {
        count_corr_numb++;
        class_numb /= 10;
    }
    return count_corr_numb;
}

int calc_max_class_numb(double numb) {
    double int_part;
    std::modf(numb, &int_part);
    int n = (int)std::abs(int_part);

    int res  = 1;
    n /= 10;

    while(n > 0) {
        n /= 10;
        res *= 10;
    }
    return res;
}

size_t task_4(double approximate_numb, double relative_err) {
    double absolute_err = task_2(approximate_numb, relative_err);

    return task_3(approximate_numb, absolute_err);
}

std::pair<double, double> task_5(double x, std::function<double(double)> func) {
    double numb = x;
    double approximate_numb = func(x);

    double absolute_err = std::abs(numb - approximate_numb);
    double relative_err = absolute_err / std::abs(approximate_numb);

    return std::pair<double, double>(absolute_err, relative_err);
}

std::pair<std::vector<double>, std::vector<double>> task_6(
        std::vector<double> exact_values,
        std::vector<double> approximate_values,
        func function,
        std::vector<func> derivatives)
{
    size_t dimension = exact_values.size();
    double absolute_err_func = std::abs(function(exact_values) - function(approximate_values));

    std::vector<double> absolut_errs;

    for(size_t i = 0; i < dimension; ++i) {
        double res = absolute_err_func / ((double)dimension * std::abs(derivatives[0](exact_values)));
        absolut_errs.push_back(res);
    }

    std::vector<double> relative_errs;
    for(size_t i = 0; i < dimension; ++i) {
        relative_errs.push_back(calc_relative_err(approximate_values[i], absolut_errs[i]));
    }

    return {absolut_errs, relative_errs};
}

double calc_relative_err(double approximate_numb, double absolute_err) {
    return absolute_err / std::abs(approximate_numb);
}


