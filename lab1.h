//
// Created by walcermelodia on 09.10.2022.
//

#ifndef NUMERICAL_METHOD_LAB1_H
#define NUMERICAL_METHOD_LAB1_H

#include "math.h"
#include "cmath"
#include <functional>
#include <vector>
double round_significant_digits(double numb, int count);

using func = std::function<double(std::vector<double>&)>;


std::pair<double, double> task_1(double numb, int count_round);

double task_2(double approximate_numb, double relative_err);

size_t task_3(double approximate_numb, double absolute_err);

size_t task_4(double approximate_numb, double relative_err);

std::pair<double, double> task_5(double x, std::function<double(double)> func);

std::pair<std::vector<double>, std::vector<double>> task_6(std::vector<double> exact_values,
                                                           std::vector<double> approximate_values,
                                                           func function,
                                                           std::vector<func> derivatives);

int calc_max_class_numb(double numb);

double calc_relative_err(double approximate_numb, double absolute_err);
#endif //NUMERICAL_METHOD_LAB1_H
