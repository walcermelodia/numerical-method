//
// Created by walce on 05.11.2022.
//

#include "lab2.h"

std::pair<matrix, vec> forward_elimination(const matrix &A, const vec &b) {
    const double EPS = 1e-9;

    matrix A_ = A;
    std::vector<double> b_ = b;

    const size_t N = A_.size();
    const size_t M = A_[0].size();

    for (size_t i = 0; i < N; ++i) {
        auto max_column_elem =
                std::max_element(A_.begin() + i, A_.end(),
                                 [&](auto a, auto b){return std::abs(a[i]) < std::abs(b[i]);});

        size_t row = std::distance(A_.begin(), max_column_elem);
        double elem = (*max_column_elem)[i];

        if (std::abs(elem) < EPS) {
            continue;
        }

        std::swap(A_[i], A_[row]);
        std::swap(b_[i], b_[row]);

        std::transform(A_[i].begin(), A_[i].end(), A_[i].begin(),
                       [&](auto x){return x / elem;});
        b_[i] = b_[i] / elem;

        for (size_t j = i + 1; j < N; ++j) {
            if (j == N) continue;

            double multiplier = A_[j][i];
            for (size_t k = i; k < M; ++k) {
                A_[j][k] -= A_[i][k] * multiplier;
            }

            b_[j] -= b_[i] * multiplier;
        }
    }
    return {A_, b_};
}

vec back_substitution(const matrix &A, const vec &b) {
    const size_t N = A.size();
    const size_t M = A[0].size();

    vec x(M);

    for (int i = M - 1; i >= 0; --i) {
        double sum = 0;
        for (size_t j = i + 1; j < M; ++j) {
            sum += A[i][j] * x[j];
        }

        x[i] = b[i] - sum;
    }
    return x;
}

vec gaussian_elimination_by_column_selection(const matrix &A, const vec &b) {
    auto[A_, b_] = forward_elimination(A, b);

    return back_substitution(A_, b_);
}
