//
// Created by walce on 05.11.2022.
//

#ifndef NUMERICAL_METHOD_LAB2_H
#define NUMERICAL_METHOD_LAB2_H


#include <vector>
#include <numeric>
using matrix = std::vector<std::vector<double>>;
using vec    = std::vector<double>;
std::pair<matrix, vec> forward_elimination(const matrix& A, const vec& b);
vec back_substitution(const matrix& A, const vec& b);
vec gaussian_elimination_by_column_selection(const matrix& A, const vec& b);


#endif //NUMERICAL_METHOD_LAB2_H
