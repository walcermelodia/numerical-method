//
// Created by walce on 06.11.2022.
//

#include "lab3.h"

std::pair<matrix, point> parse(const matrix &A, const point &f) {
    matrix B = A;
    point f_ = f;
    for (size_t i = 0; i < B.rows(); ++i) {
        double elem = B[i][i];

        for (size_t j = 0; j < B.cols(); ++j) {
            B[i][j] /= elem * -1;
        }
        f_[i] /= elem;

        B[i][i] = 0.0;
    }

    return {B, f_};
}

point jacobi_method(const matrix &A, const point &f, double eps) {
    auto[B, C] = parse(A, f);

    double norm_b = B.norm();
    double norm_c = C.norm();

    point diagA = A.getDiag();
    point x_prev = C;
    point x_next = x_prev;

    int iter = count_iter(norm_b, norm_c, eps);
    for (int i = 0; i < iter; ++i) {
        point subtract = calc_subtract(diagA, x_prev);

        x_next = A * x_prev - subtract + f;
        x_next = calc_divider(diagA, x_next);
        x_prev = x_next;
    }
    return x_prev;
}

point calc_subtract(const point& diag, const point& p) {
    point res = diag;

    for (size_t i = 0; i < diag.dimension(); ++i) {
        res[i] *= p[i];
    }
    return res;
}

int count_iter(double norm_b, double norm_c, double eps) {
    return (std::log(eps) + std::log(1 - norm_b) - std::log(norm_c)) / std::log(norm_b) - 1;
}

point calc_divider(const point &diag, const point &p) {
    point res = diag;

    for (size_t i = 0; i < diag.dimension(); ++i) {
        res[i] *= - 1.0 / p[i];
    }
    return res;
}
