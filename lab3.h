//
// Created by walce on 06.11.2022.
//

#ifndef NUMERICAL_METHOD_LAB3_H
#define NUMERICAL_METHOD_LAB3_H
#include "point.h"
#include "matrix.h"
#include "cmath"

std::pair<matrix, point> parse(const matrix& A, const point& f);
int count_iter(double norm_b, double norm_c, double eps);
point calc_subtract(const point& diag, const point& p);
point calc_divider(const point& diag, const point& p);
point jacobi_method(const matrix& A, const point& f, double eps);
#endif //NUMERICAL_METHOD_LAB3_H
