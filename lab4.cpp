//
// Created by walce on 06.11.2022.
//

#include "lab4.h"

double calc_by_lagrange_formula(std::vector<double> x, std::vector<double> f_x, double point) {
    std::vector<double> A(x.size());

    for (size_t i = 0; i < A.size(); ++i) {
        double product = 1.0;
        for (size_t j = 0; j < x.size(); ++j) {
            if (j == i) continue;

            product *= x[i] - x[j];
        }

        A[i] = 1.0 / product;
    }

    double sum_numerator = 0.0;
    double sum_denominator = 0.0;

    for (size_t k = 0; k < x.size(); ++k) {
        sum_numerator += A[k] * f_x[k] / (point - x[k]);
        sum_denominator += A[k] / (point - x[k]);
    }

    return sum_numerator / sum_denominator;
}
