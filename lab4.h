//
// Created by walce on 06.11.2022.
//

#ifndef NUMERICAL_METHOD_LAB4_H
#define NUMERICAL_METHOD_LAB4_H

#include <vector>

double calc_by_lagrange_formula(std::vector<double> x, std::vector<double> f_x, double point);


#endif //NUMERICAL_METHOD_LAB4_H
