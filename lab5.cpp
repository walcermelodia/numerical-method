//
// Created by walce on 06.11.2022.
//

#include "lab5.h"

std::pair<std::vector<double>, std::vector<double>> forward_elimination(const matrix &A, const point &f) {
    const size_t M = A.rows();
    std::vector<double> X(M - 1);
    std::vector<double> Z(M - 1);

    X[0] = - A[0][1] / A[0][0];
    Z[0] = f[0] / A[0][0];

    double a, b, c;
    for (size_t i = 1; i < X.size(); ++i) {
        a = A[i][i + 1];
        b = A[i][i];
        c = A[i][i - 1];

        X[i] = - a / (b + c * X[i - 1]);
        Z[i] = (f[i] - c * Z[i - 1]) / (b + c * X[i - 1]);
    }

    return {X, Z};
}

point back_substitution(const matrix &A, const point &f, std::vector<double> X, std::vector<double> Z) {
    const size_t M = A.rows();

    point res(M);
    res[M - 1] = (f[M - 1] - A[M - 1][M - 2] * Z[M - 2]) / (A[M - 1][M - 1] + A[M - 1][M - 2] * X[M - 2]);
    for (int i = M - 2; i >= 0; --i) {
        res[i] = X[i] * res[i + 1] + Z[i];
    }

    return res;
}

point tridiagonal_matrix_right_hand_algorithm(const matrix &A, const point &f) {
    auto[X, Z] = forward_elimination(A, f);

    return back_substitution(A, f, X, Z);
}

std::pair<matrix, point> create_tridiagonal_matrix(const std::vector<double> &h,
                                                   const std::vector<double> &f_x,
                                                   double alpha, double beta) {
    matrix m(5);
    point f(5);

    m[0][0] = 4 * h[1] + 3 * h[0];
    m[0][1] = h[1];
    m[4][3] = 4;
    m[4][4] = 3;
    f[0] = 12 * ((f_x[2] - f_x[1]) / h[1] - (f_x[1] - f_x[0]) / h[0]) - h[0] * alpha;
    f[4] = 2 * beta;

    for (size_t i = 1; i < m.rows() - 1; ++i) {
        m[i][i - 1] = h[i - 1];
        m[i][i]     = 2 * (h[i] + h[i - 1]);
        m[i][i + 1] = h[i];

        f[i] = 6 * ((f_x[i + 2] - f_x[i + 1]) / h[i] - (f_x[i + 1] - f_x[i]) / h[i - 1]);
    }

    return {m, f};
}

std::vector<cubic_polynomial> create_polynomials(const std::vector<double> &x,
                                                 const point &c,
                                                 const std::vector<double> &h,
                                                 const std::vector<double> &f_x,
                                                 double alpha) {
    std::vector<double> a = f_x;
    std::vector<double> b(a.size());
    std::vector<double> d(a.size());

    b[0] = (f_x[1] - f_x[0]) / h[0] + (3 * h[0] * c[0] + h[0] * alpha) / 12;
    d[0] = (3 * c[0] - alpha) / (2 * h[0]);

    for (size_t i = 1; i < b.size(); ++i) {
        b[i] = (f_x[i + 1] - f_x[i]) / h[i] + (2 * h[i] * c[i] + h[i] * c[i - 1]) / 6;
        d[i] = (c[i] - c[i - 1]) / h[i];
    }

    std::vector<cubic_polynomial> polynomials(5);
    for (size_t i = 0; i < polynomials.size(); ++i) {
        polynomials[i] = cubic_polynomial({a[i], b[i], c[i], d[i], x[i]});
    }

    return polynomials;
}

point cubic_spline_interpolation(const std::vector<double> &x,
                                 const std::vector<double> &f_x,
                                 double alpha, double beta, double p) {
    std::vector<double> h(5);

    for (size_t i = 0; i < h.size(); ++i) {
        h[i] = x[i + 1] - x[i];
    }

    auto[A, f] = create_tridiagonal_matrix(h, f_x, alpha, beta);
    auto c = tridiagonal_matrix_right_hand_algorithm(A, f);

    std::vector<cubic_polynomial> polynomials = create_polynomials(x, c, h, f_x, alpha);

    point value(5);
    for (size_t i = 0; i < value.dimension(); ++i) {
        value[i] = polynomials[i](p);
    }

    return value;
}


























