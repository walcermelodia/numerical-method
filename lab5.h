//
// Created by walce on 06.11.2022.
//

#ifndef NUMERICAL_METHOD_LAB5_H
#define NUMERICAL_METHOD_LAB5_H

#include "point.h"
#include "matrix.h"
#include <vector>
#include "cubic_polynomial.h"

std::pair<std::vector<double>, std::vector<double>> forward_elimination(const matrix& A, const point& f);
point back_substitution(const matrix& A, const point& f, std::vector<double> X, std::vector<double> Z);
point tridiagonal_matrix_right_hand_algorithm(const matrix& A, const point& f);
std::pair<matrix, point> create_tridiagonal_matrix(const std::vector<double>& h,
                                                   const std::vector<double>& f_x,
                                                   double alpha, double beta);
std::vector<cubic_polynomial> create_polynomials(const std::vector<double>& x,
                                                 const point& c,
                                                 const std::vector<double>& h,
                                                 const std::vector<double>& f_x,
                                                 double alpha);
point cubic_spline_interpolation(const std::vector<double>& x,
                                 const std::vector<double>& f_x,
                                 double alpha, double beta, double p);

#endif //NUMERICAL_METHOD_LAB5_H
