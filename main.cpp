#include <iostream>
#include <iostream>
#include <algorithm>
#include <numeric>
#include <cmath>
#include "lab5.h"
#include "point.h"
#include "matrix.h"

template<typename K>
void print_vector(const std::vector<K> &v) {
    for(auto now : v) {
        std::cout << now << " ";
    }
    std::cout << std::endl;
}


void print_point(const point &p) {
    for (size_t i = 0; i < p.dimension(); ++i) {
        std::cout << p[i] << " ";
    }
    std::cout << std::endl;
}

/*void print_matrix(const matrix m) {
    for (size_t i = 0; i < m.rows(); ++i) {
        for (auto elem : m[i]) {
            std::cout << elem << " ";
        }
        std::cout << std::endl;
    }
}*/


int main() {
    /*//lab 2
    matrix A = {{-1, 3, 1}, {2, -6, -1}, {2, 5, 3}};
    std::vector<double> b = {4, 3, 1};

    vec res = gaussian_elimination_by_column_selection(A, b);

    print_vector(res);*/

    /*//lab3
    matrix A( { {25, 1, -3.5}, {0, -9.4, 3.4}, {1, -1, 7.3} } );
    point f( {5, 3, 0} );
    auto res = jacobi_method(A, f, 0.001);

    print_point(res);*/


    /*//lab4
    std::vector<double> x = {2.0, 2.3, 2.5, 3.0, 3.5, 3.8, 4.0};
    std::vector<double> f_x = {5.848, 6.127, 6.3, 6.694, 7.047, 7.243, 7.368};
    double  point = 2.78;
    auto res = calc_by_lagrange_formula(x, f_x, point);
    std::cout << res << std::endl;*/


    //lab5
    /*std::vector<std::vector<double>> v = { {-3, 2, 0, 0, 0},
                                           {1, 3, -2, 0, 0},
                                           {0, 2, 6, 3, 0},
                                           {0, 0, -7, 9, 1},
                                           {0, 0, 0, 8, 1}
    };

    std::vector<double> vv = {1, 4, 0, -3, -7};*/

    /*std::vector<std::vector<double>> v = { {2, 1, 0, 0, 0},
                                           {1, 3, 1, 0, 0},
                                           {0, 1, 4, 2, 0},
                                           {0, 0, 2, 5, 2},
                                           {0, 0, 0, 2, 1}
    };

    std::vector<double> vv = {1, 2, -1, 0, 3};


    matrix A(v);
    point f(vv);

    auto res = tridiagonal_matrix_right_hand_algorithm(A, f);
    print_point(res);*/

    std::vector<double> x = {0.1, 0.14, 0.16, 0.2, 0.24, 0.3};
    std::vector<double> f_x = {0.1234, 0.1456, 0.1874, 0.2361, 0.2475, 0.4562};
    double alpha = -0.3421;
    double beta = -0.6578;
    double point = 0.2;
    auto res = cubic_spline_interpolation(x, f_x, alpha, beta, point);

    print_point(res);
    return 0;
}























