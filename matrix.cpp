//
// Created by walce on 06.11.2022.
//
#include "matrix.h"

matrix::matrix(size_t size) {
    data.assign(size, std::vector<double>(size));
}

matrix::matrix(size_t rows, size_t cols) {
    data.assign(rows, std::vector<double>(cols));
}

matrix::matrix(const std::vector<std::vector<double>> &matrix) {
    data = matrix;
}

std::vector<double> &matrix::operator[](size_t row) {
    return data[row];
}

const std::vector<double> &matrix::operator[](size_t row) const {
    return data[row];
}

size_t matrix::rows() const {
    return data.size();
}

size_t matrix::cols() const {
    return data[0].size();
}

matrix operator*(const matrix &m, double scalar) {
    matrix res(m.data);

    for (auto& row : res.data) {
        for (auto& elem : row) {
            elem *= scalar;
        }
    }
    return res;
}

double matrix::norm() const {
    double res = std::accumulate(this->data[0].begin(), this->data[0].end(), 0,
                                 [](auto a, auto b){return std::abs(a) + std::abs(b);});

    for (size_t i = 1; i < this->rows(); ++i) {
        double tmp = std::accumulate(this->data[i].begin(), this->data[i].end(), 0,
                                     [](auto a, auto b){return std::abs(a) + std::abs(b);});
        res = std::max(res, tmp);
    }

    return res;
}

point operator*(const matrix &m, const point &p) {
    point res(p.dimension());
    std::vector<double> v = p.getData();

    for (size_t i = 0; i < p.dimension(); ++i) {
        res[i] = std::inner_product(m[i].begin(), m[i].end(), v.begin(), 0);
    }

    return res;
}

point matrix::getDiag() const {
    point res(this->rows());

    for (size_t i = 0; i < this->rows(); ++i) {
        res[i] = this->data[i][i];
    }

    return res;
}









