//
// Created by walce on 06.11.2022.
//

#ifndef NUMERICAL_METHOD_MATRIX_H
#define NUMERICAL_METHOD_MATRIX_H

#include <vector>
#include <numeric>
#include "point.h"

class matrix {
private:
    std::vector<std::vector<double>> data;

public:
    matrix(size_t size = 0);
    matrix(size_t rows, size_t cols);
    matrix(const std::vector<std::vector<double>>& matrix);

    std::vector<double>& operator[](size_t row);
    const std::vector<double>& operator[](size_t row) const;

    size_t rows() const;
    size_t cols() const;
    double norm() const;
    point getDiag() const;

    friend matrix operator*(const matrix& m, double scalar);
    friend point operator*(const matrix& m, const point& p);
};

#endif //NUMERICAL_METHOD_MATRIX_H
