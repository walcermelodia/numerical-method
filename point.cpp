//
// Created by walce on 06.11.2022.
//

#include "point.h"
#include <algorithm>

point::point(size_t n) {
    data.assign(n, 0);
}

point::point(const std::vector<double> &point) {
    data = point;
}

size_t point::dimension() const {
    return data.size();
}

double &point::operator[](size_t pos){
    return data[pos];
}

const double &point::operator[](size_t pos) const {
    return data[pos];
}

double point::norm() const {
    return std::abs(*std::max_element(this->data.begin(), this->data.end(),
                            [](auto a, auto b){return std::abs(a) < std::abs(b);}));
}

const std::vector<double>& point::getData() const {
    return data;
}

point operator-(const point &p1, const point &p2) {
    point res = p1;

    for (size_t i = 0; i < p1.dimension(); ++i) {
        res[i] -= p2[i];
    }
    return res;
}

point operator+(const point &p1, const point &p2) {
    point res = p1;

    for (size_t i = 0; i < p1.dimension(); ++i) {
        res[i] += p2[i];
    }
    return res;
}
