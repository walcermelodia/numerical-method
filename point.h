//
// Created by walce on 06.11.2022.
//

#ifndef NUMERICAL_METHOD_POINT_H
#define NUMERICAL_METHOD_POINT_H
#include <vector>

class point {
private:
    std::vector<double> data;

public:
    point(size_t n = 0);
    point(const std::vector<double>& point);

    size_t dimension() const;
    double& operator[](size_t pos);
    const double& operator[](size_t pos) const;

    double norm() const;
    const std::vector<double>& getData() const;

    friend point operator-(const point& p1, const point& p2);
    friend point operator+(const point& p1, const point& p2);
};


#endif //NUMERICAL_METHOD_POINT_H
